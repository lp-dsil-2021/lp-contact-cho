import org.jetbrains.annotations.NotNull;

import java.util.List;
public class ContactDAO implements IContactDAO {
    private List<Contact> contacts;

    public void add(Contact contact){
        contacts.add(contact);
    }

    public boolean isContactExist(String name){

        String trimmedName = name.trim();
        contacts.stream().anyMatch(x->trimmedName.equalsIgnoreCase(x.getName()));

        //Java 5 - Old Fashioned way
        // Simuler mon select where trimmedName=""
        /*for (Contact contact : contacts){
            if(trimmedName.equalsIgnoreCase(contact.getName())){
                return  true;
            }
        }
        return  true;*/
        boolean res = false;
        if(contacts.size() > 0) {

            for (int i = 0; i < contacts.size(); i++) {
                if (trimmedName.equalsIgnoreCase(contacts.get(i).getName())) {
                    return res = true;
                }
            }
        }
        return res;
    }

    public boolean isContactExist(String name,String surname){
        String trimmedSurname = surname.trim();
        String trimmedName = name.trim();
        contacts.stream().anyMatch(x->trimmedName.equalsIgnoreCase(x.getName()));
        contacts.stream().anyMatch(x->trimmedSurname.equalsIgnoreCase(x.getSurname()));
        //Java 5 - Old Fashioned way
        // Simuler mon select where trimmedName=""
        /*for (Contact contact : contacts){
            if(trimmedName.equalsIgnoreCase(contact.getName())){
                return  true;
            }
        }
        return  true;*/
        boolean res = false;
        if(contacts.size() > 0) {

            for (int i = 0; i < contacts.size(); i++) {
                if (trimmedName.equalsIgnoreCase(contacts.get(i).getName())&&trimmedSurname.equalsIgnoreCase(contacts.get(i).getSurname())) {
                    return res = true;
                }
            }
        }
        return res;
    }
}
