public class ContactService implements IContactService{
    private  IContactDAO contactDao = new ContactDAO();

    public void creerContact(String nom) throws ContactException{
        String trim_nom = nom.trim();
        if (trim_nom == null || trim_nom == "" || trim_nom.length() > 40 || trim_nom.length() < 3 || contactDao.isContactExist(trim_nom)){
            throw new ContactException();
        }
        Contact c = new Contact(trim_nom);
        contactDao.add(c);
    }
    public void creerContact(String name,String surname) throws ContactException{
        String trimmedName = name.trim();
        String trimmedSurname = surname.trim();
        if (trimmedName == null ||trimmedSurname== null || trimmedSurname=="" || trimmedSurname.length() > 40 || trimmedSurname.length() < 3 || trimmedName == "" || trimmedName.length() > 40 || trimmedName.length() < 3 || contactDao.isContactExist(trimmedName,trimmedSurname)){
            throw new ContactException();
        }
        Contact c = new Contact(trimmedName,trimmedSurname);
        contactDao.add(c);
    }
}