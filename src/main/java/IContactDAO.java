public interface IContactDAO {
    void add(Contact unContact) throws ContactException;
    boolean isContactExist(String name) throws ContactException;
    boolean isContactExist(String name,String surname) throws ContactException;
}
