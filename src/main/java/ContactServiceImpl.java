import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ContactServiceImpl implements IContactService {
    @Override
    public void creerContact(String name) throws ContactException {
        String trimmedName=name.trim();
       if(trimmedName == null || trimmedName.trim().length() < 3 || trimmedName.trim().length() > 40 || getSpecialCharacterCount(trimmedName)==0){
           throw new ContactException();
       }
    }

    @Override
    public void creerContact(String name, String surname) throws ContactException {
        String trimmedName = name.trim();
        String trimmedSurname = surname.trim();
        if(trimmedName == null || trimmedName.trim().length() < 3 || trimmedName.trim().length() > 40 || getSpecialCharacterCount(trimmedName)==0){
            throw new ContactException();
        }
    }

    private int getSpecialCharacterCount(String s) {
        int res=1;

        Pattern letter = Pattern.compile("[a-zA-z]");
        Pattern digit = Pattern.compile("[0-9]");
        Pattern special = Pattern.compile ("[!@#$%&*()_+=|<>?{}\\[\\]~-]");


        Matcher hasLetter = letter.matcher(s);
        Matcher hasDigit = digit.matcher(s);
        Matcher hasSpecial = special.matcher(s);
        if(hasDigit.find() || hasSpecial.find()) {
            res=0;
        }
        return res;
    }
}
