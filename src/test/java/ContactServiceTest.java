import org.junit.Test;

public class ContactServiceTest {

    IContactService service = new ContactServiceImpl();

    @Test(expected = ContactException.class)
    public void shouldFailNull() throws ContactException{
         service.creerContact(null);
    }
    @Test(expected = ContactException.class)
    public void shouldFailLessThanThree() throws ContactException{
        service.creerContact("ab");
    }
    @Test(expected = ContactException.class)
    public void shouldFailIfBlank() throws ContactException{
        service.creerContact("   ");
    }
    @Test()
    public void shouldSuccedWithThree() throws ContactException{
        service.creerContact("abc");
    }
    @Test(expected = ContactException.class)
    public void shouldFailWithMoreThanFourty() throws ContactException{
        service.creerContact("azertyuiopqsdfghjklmwxcvbnazertyuiopqsdfghjklwxcvbnazertyuiopqsdfghjklwxcvbn");
    }
    @Test(expected = ContactException.class)
    public void shouldFailIfContainNumericOrSpecial() throws ContactException{
        service.creerContact("#{$2456");
    }

}
